module.exports = {
    title: 'Le masque et la plume',
    description: 'Support en ligne pour le cours arts numériques b1',
    head: [
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js' }],
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i&display=swap' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap' }],
  ],
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            
            { text: 'Digitalab', link: 'http://digitalab.be' }
        ],
        sidebarDepth: 0,
        sidebar: [

            {
              title: 'Généralités',
              collapsable: false,
              children: [
                'generalites/introduction',
                'generalites/formulaire',
              ]
            },
            {
              title: 'Vectoriel',
              collapsable: false,
              children: [
                'vectoriel/formes-simples',
                'vectoriel/alignements',
                'vectoriel/plume',
                'vectoriel/the-shapes-project',
                'vectoriel/pathfinder-et-masques',
                'vectoriel/motifs',
              ]
            },
            {
            title: 'Bitmap',
            collapsable: false,
            children: [
                'bitmap/selections',
                'bitmap/masques',
                'bitmap/reglages',
                'bitmap/retouches',
            ]
            },
            {
                title: 'Mise en page',
                collapsable: false,
                children: [
                    'mise-en-page/gabarits',
                    'mise-en-page/images',
                ]
                },
            {
              title: 'Exercices',
              children: [ /* ... */ ]
              }
          ]
    }
}