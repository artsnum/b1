---
title: Les retouches
lang: fr-FR
---

# Réglages & retouches

<a data-fancybox title="Jacques Perconte" href="/assets/perconte.png">![Jacques Perconte](/assets/perconte.png)</a>

## Retour sur les calques de réglages

<a data-fancybox title="Anne-Laure Etienne" href="/assets/annelaureetienne.png">![Anne-Laure Etienne](/assets/annelaureetienne.png)</a>

> http://www.unspokenimage.com/untitled-gallery#6

http://www.unspokenimage.com/untitled-gallery#5 (Eden Millon)

## Nous savons:

- Ce que sont résolution, définition et taille d'image.
- Le fonctionnement des calques dans Photoshop
- Transformer un arrière-plan en calque
- Renommer un calque / grouper des calques (+ **aplatir une image**)
- Importer/copier une image dans un document existant (+objets dynamiques).
- Utiliser les outils de sélection de base (Sélection rectangulaire, lasso, etc..)
- Créer un tracé vectoriel et le transormer en sélection (ou inversément)
- Ajouter et utiliser un masque de fusion
- Ajouter et utiliser un calque de réglage
- Ajouter et utiliser un calque de remplissage

## Aujourd'hui

- Travaux rendus (+ mises au point)
- Outils de retouche: tampon de duplication
- Outil correcteur et correcteur localisé
- Outil Pièce

## A faire au cours

<a data-fancybox title="Retouche" href="/assets/retouche1.jpg">![Retouche](/assets/retouche1.jpg)</a>
<a data-fancybox title="Retouche" href="/assets/retouche2.jpg">![Retouche](/assets/retouche2.jpg)</a>

- Nettoyer une image
- Dupliquer une fenêtre
- Effacer quelqu'un d'une image
- Filtre fluidité
- Exercice récapitulatif Georges Rousse

[Anthony Gormley, photo originale](/assets/gormley01.jpg)

<a data-fancybox title="Anthony Gormley" href="/assets/gormley02.jpg">![Anthony Gormley](/assets/gormley02.jpg)</a>

<a data-fancybox title="Retouches" href="/assets/retouches02.jpg">![Retouches](/assets/retouches02.jpg)</a>

<a data-fancybox title="Retouches" href="/assets/retouches01.jpg">![Retouches](/assets/retouches01.jpg)</a>

<a data-fancybox title="Retouches" href="/assets/retouches03.jpg">![Retouches](/assets/retouches03.jpg)</a>

<a data-fancybox title="India_Lawton" href="/assets/2013_03_27_India_Lawton_16.jpg">![India_Lawton](/assets/2013_03_27_India_Lawton_16.jpg)</a>

[Façade 01](/assets/facade01.jpg)
[Façade 01](/assets/facade02.jpg)

## Architectures

- Philippe de Gobert

- Filip Dujardin

## [Walter de Maria](/assets/02_walterDeMaria.zip)##

<a data-fancybox title="Résultat" href="/assets/RESULTAT.jpg">![Résultat](/assets/RESULTAT.jpg)</a>
