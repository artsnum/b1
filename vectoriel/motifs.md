---
title: Les motifs
lang: fr-FR
---

# Les motifs

<a data-fancybox title="" href="/assets/viallat.jpg">![](/assets/viallat.jpg)</a>

+ Retour sur la cartographie
+ Les tracés transparents
+ Principe des motifs
+ Dégradés de couleurs et filets de dégradés
+ Principe des motifs
+ Point d'origine des motifs
+ Créer un motif
+ Modifier un motif
+ Création d'un motif simple, sans raccordement
+ https://amandabaum.com/TEXTILE-PATTERNS

<a data-fancybox title="" href="/assets/motif10.png">![](/assets/motif10.png)</a>

## Marble floor

A partir du travail *Marble floor* (1999) de Wim Delvoye:

1. Recréez un des deux motifs repris ci-dessous. 
2. A l'aide des masques d'écrêtage, modifiez-le pour en faire une version 'Charcuterie'

<a data-fancybox title="" href="/assets/delvoye1.jpg">![](/assets/delvoye1.jpg)</a>
<a data-fancybox title="" href="/assets/delvoye2.jpg">![](/assets/delvoye2.jpg)</a>

----

## Vuittami

A l'instar de <a data-fancybox title="Takashi Murakami" href="https://www.google.be/search?rlz=1C5CHFA_enBE752BE753&biw=1309&bih=653&tbm=isch&sa=1&ei=ulzAW6nYKtDVwQKNobLwDw&q=takashi+murakami&oq=tamurakami&gs_l=img.3.0.0i7i30k1l10.7763.14390.0.16216.8.6.2.0.0.0.81.373.6.6.0....0...1c.1.64.img..0.8.382...0j35i39k1j0i67k1j0i30k1j0i8i7i30k1.0.SqBT5aeXmqY">![Takashi Murakami](https://www.google.be/search?rlz=1C5CHFA_enBE752BE753&biw=1309&bih=653&tbm=isch&sa=1&ei=ulzAW6nYKtDVwQKNobLwDw&q=takashi+murakami&oq=tamurakami&gs_l=img.3.0.0i7i30k1l10.7763.14390.0.16216.8.6.2.0.0.0.81.373.6.6.0....0...1c.1.64.img..0.8.382...0j35i39k1j0i67k1j0i30k1j0i8i7i30k1.0.SqBT5aeXmqY)</a>, créez votre version personnelle du motif Louis Vuitton, à partir du document ci-dessous.

<a data-fancybox title="" href="/assets/vuitton.png">![](/assets/vuitton.png)</a>
<a data-fancybox title="" href="/assets/murakami.jpg">![](/assets/murakami.jpg)</a>

Dans les deux cas, il vous faudra être méthodiques et précis (Alignements, groupes, pathfinder, etc..)
Veillez à sauver régulièrement votre travail.

-----

Au final, votre image ne doit comporter que 3 objets:

+ L'appareil photo
+ Le cercle blanc
+ Le fond jaune

<a data-fancybox title="" href="/assets/photo1.png">![](/assets/photo1.png)</a>
<a data-fancybox title="" href="/assets/photo2.png">![](/assets/photo2.png)</a>
<a data-fancybox title="" href="/assets/photo3.png">![](/assets/photo3.png)</a>

---- 

+ [Exercice alignements](/assets/alignements.pdf)

<a data-fancybox title="" href="/assets/dominos.png">![](/assets/dominos.png)</a>