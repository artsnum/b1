---
title: Dessin à la plume
lang: fr-FR
---

# Dessin à la plume

<a data-fancybox title="Alex Katz" href="/assets/Alex-Katz_Brisk-Day.jpg">![Alex Katz](/assets/Alex-Katz_Brisk-Day.jpg)</a>
[Alex Katz](https://www.google.be/search?q=alex+katz&rlz=1C5CHFA_enBE752BE753&source=lnms&tbm=isch&sa=X&ved=0ahUKEwitor7yy_zYAhWMyKQKHd-vAbsQ_AUICigB&biw=1307&bih=709#imgrc=_), Brisk Day, 1990 (Xylogravure / Aquatinte / Lithographie)

+ https://www.desmos.com/calculator/cahqdxeshd
+ https://bezier.method.ac/
+ http://math.hws.edu/eck/cs424/notes2013/canvas/bezier.html

## Au programme

+ Récapitulatif des 2 premiers cours
+ Elsworth Kelly
+ Retour sur les contours : la fenêtre contours + options
+ Qu’est-ce que la plume?
+ Les courbes de Bézier
+ Manipulation de points et outil de sélection directe
+ Tracés ouverts et fermés+ Jonctions de tracés
+ Tracés contraints
+ Dessin à la plume: principaux raccourcis clavier
+ Transformations : Miroirs

<a data-fancybox title="Canada flag" href="/assets/canada.png">![Canada flag](/assets/canada.png)</a>
<a data-fancybox title="Fleur de lys" href="/assets/750px-Fleur_de_lys_du_quebec.png">![Fleur de lys](/assets/750px-Fleur_de_lys_du_quebec.png)</a>
<a data-fancybox title="Québec flag" href="/assets/quebec.png">![Québec flag](/assets/quebec.png)</a>